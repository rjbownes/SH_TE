# Technical Assesment 

This is a challenge to create a model for the prediction of stroke in labeled patient data. We have several continuous and categorical variables to use to generate the model and create accurate predictions. The table below highlights each of these features and the possible values for each feature.

![](data/picture.png)

### Features and missing data

This is a fairly complete and well annotated dataset except for the features of BMI and smoking status. BMI is missing 1462 entries or about ~3% of the data and there are 13292 missing values for smoking status representing ~30% of the samples. BMI can safely be imputed with the median value (27.7) to refill the data, but smoking is missing too many values for safe or reasonable imputation. This means we either need to remove the feature column, or remove every sample with out that feature. I'll take a look at this in a multivariate analysis to finalize that decision.

### Feature evaluation

Initial evaluation suggests that some of these variables will be of tremendous value to a model; age, hypertensive status, smoking status, bmi and glucose level. Other factors like heart disease will possibly be good indicators but will likely be co-linear with other factors. Residence type, marital status and wok type could help create a deprivation index or meta score but are likely going to contribute as much noise as signal in training. To test this, I will make an 80:20 split and make a GLM model and inspect the values that are most important for stroke prediction under multivariate analysis.

    
 | Coefficients        | Estimate           | Std.Err  | Z-val | P |
| ------------- |:-------------:| -----:| :-------------: | :-------------: |
| genderMale | -1.957e+01 |  2.881e+02 |  -0.068 | 0.945844 |
| genderOther | -9.219e-08 |  2.165e-06 |  -0.043 | 0.966042 |
| age | 7.220e-02 |  4.022e-03 |  17.953 |  < 2e-16 *** |
| hypertension | 4.032e-01 | 1.040e-01 |  3.876 | 0.000106 *** |
| heart_disease | 6.690e-01 | 1.155e-01  | 5.792 | 6.97e-09 *** |
| avg_glucose_level | 3.801e-03 | 8.024e-04 |  4.737 | 2.17e-06 *** |
| bmi | -1.085e-02 | 7.675e-03 | -1.414 | 0.157320 |
| never_smoked |-2.670e-02 | 1.062e-01 | -0.251 | 0.801564 |
| smokes | 2.237e-01 | 1.300e-01  | 1.721 | 0.085205 |

Additionally, we can see from the above abbreviate GLM result that some features are indeed less useful, like gender, but the expected physiological observations of hypertension and glucose are highly significant. BMI suprising doesn't clear the 0.05 p value cut off but is still relatively valuable, the same with smoking, however positive smoking history trends toward significance and it is probably best to eliminate the samples missing this feature instead of eliminate the feature. 

### Class imbalance and sampling methods

Due to the presence of categorical variables, the data is next encoded with full-ranked dummy variables to ensure we do not add co-linearity to multi-level features. The stroke target class is highly imbalanced in this data, which means we need to adjust for this and use an appropriate accuracy metric to evaluate our models. The data is treated with four different subsampling methods to create class balance: down sampling, up sampling, ROSE and SMOTE. Down sampling reduces the class size of both outcomes to the size of the minority class, while up sampling re-samples the minority class with replacement to match the majority class. ROSE and SMOTE are both methods for creating artificial minority class samples to create balance. I will run a simple bagging ML method on each instance of sampling and test the resulting method with ROC/AUC as the test metric to identify the most performant method for class balance.

#### Internal performance

| sampling | Min. | 1st Qu. |  Median  |   Mean  | 3rd Qu.   |   Max.| NA's |
| ---------- |:----:| ----:| :----: | :--: | :---: | :---: | :--: |
| original | 0.723360 |0.7264956 |0.7296313 |0.7296313 |0.7327670 |0.7359026   | 0
| down |     0.786232 |0.7926388 |0.7990456| 0.7990456| 0.8054523 |0.8118591    | 0
| up |       1.000000 |1.0000000 |1.0000000 |1.0000000| 1.0000000 |1.0000000|    0
| SMOTE |    0.913229 |0.9167962 |0.9203634 |0.9203634| 0.9239306| 0.9274979|    0
| ROSE |     1.000000 |1.0000000 |1.0000000 |1.0000000| 1.0000000 |1.0000000|0

Re-sampling on the training data shows shockingly good performance for up sampling and ROSE, let's see how they do in validation.

| sampling | lower | ROC | upper |
| ---------- |:----:| ----:| :----: | 
original | 0.6728402 | 0.7179544 | 0.7630686
down |0.7665880 | 0.7963549 | 0.8261217 |
up |0.6780233 | 0.7204411 | 0.7628588 |
SMOTE | 0.7673168 | 0.7997604 | 0.8322039 |
ROSE | 0.5009541 | 0.5016964 | 0.5024386 |

In testing, the SMOTE class balancing method gave the best ROC/AUC performance. I will be moving forward with data that has been subset to remove the samples with smoking data removed, median inputed BMI values, and class balanced with the SMOTE algorithm.

### ML methods

Machine learning methods do not exist in a vacuum and just because one algorithm works well this doesn't mean that other methods are not comparatively better. Therefore, it is often appropriate to test multiple algorithms to check for relative performance. In python, there are tools like Teapot to simultaneously perform multiple testing and hyper parameterization but at significant computational and time expense. I will be be performing the ML analysis here using R, caret and three methods that perform analysis in different ways to see if any one method predominates. I have also chosen this set up due to the fact that R, especially with tidyverse, is quicker for data exploration and preprocessing, which is ideal for timed tasks. Caret is also very easy to run in parallel, as I won't have access to a server and don't want to assume that my submission would be run on anything more powerful than my own laptop, this will ensure the models and code are portable. 

The first ML method is RDA, which is similar in execution to ANOVA and identifies the features which most powerfully separate naturally occurring classes. The second method, Randomforest, is an ensemble learning method which pools the results of many smaller, worse, tests and aggregates the results to come to a consensus result. RF is a workhorse of a machine learning algorithm and is a common reference metric for performance. Lastly, as a true benchmark for performance, we will look at a probabilistic model, Naive Bayes classifier. This will be by far the fastest model to fit and it will be interesting to evaluate the increase in performance the other models attain at the cost of run time.

### Model Performance

The below table summarises the results for the three methods. RDA and Randomforest had very similar performance at about 86% AUC. The Bayesian method outperformed both methods with ~90% AUC and was additionally significantly faster with a ~200x speed up in the Bayes model when compared to RDA and RF (two minutes reduced to ~1 second). It is impossible to say we would see the same result on data from an entirely different distribution as the test.csv file contained on the [Kaggle](https://www.kaggle.com/asaumya/healthcare-dataset-stroke-data/downloads/healthcare-dataset-stroke-data.zip/1) page was missing the target column so this could only be tested on the validation split of the training data.   

| Method | Training | Testing | 
| ---------- |:----:| ----:| 
**RDA**  | 0.8323739 |0.8644
**RF**  | 0.8213954 |0.8597
**NB**  | 0.8234250 | 0.9066715
 

## Results and Conclusion

The three methods all had reasonable performance, however the Bayesian model was both more accurate and faster to train. The preprocessing and data preparation probably contributed to the concordance of the RDA and RF results in this regard. Overall, fixing class balance with SMOTE and retaining full feature length with minimal imputation resulted in three ML models with good ROC scores for the prediction of stroke. The chosen models were all tuned with hyperparameterization but with relatively few parameters and using random search to save training time. Wider grid search of many more parameters would result in greater model accuracy and is a valid avenue of improvement. Further model testing, including ensembled models of these three models and neural net approaches with TF/Keras, also offer possible improvement routes to these results.
