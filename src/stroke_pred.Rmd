---
title: "R Notebook"
output: html_notebook
editor_options: 
  chunk_output_type: inline
---

First some libraries

```{r}
library(tidyverse)
library(caret)
library(ROSE)
library(doParallel)
library(DMwR)
library(pROC)
```

Load the data and check to see if there are NA's

```{r}
df <- read_csv("../data/train_2v.csv")
df %>% summary
```

Right off the back we see there are 1462 NA's for BMI, and given only my current understanding of stroke risk that is one important metric for measureing risk. This represents only ~3% of the total samples, so I will impute the median value for the class. This will add some uncertainty to the model, but given a low volume on missing data is probably the most appropriate way to handle the column.


Additionally, there are 13292 smokers with no status listed, anoter important variable. This is unfortunate as that represents ~30% of the total data. Imputing this would be irresponsible as it would totally transform the data. I will have to make a decision on whether to drop this column from analysis, or subset by the complete data.

```{r}
med_bmi <-  median(df$bmi,na.rm = T)
df <- df %>%
        mutate(bmi = ifelse(is.na(bmi),med_bmi,bmi))

df <- df %>% 
  filter(smoking_status != "")
```

Train/test split

```{r}
trainIndex <- createDataPartition(df$stroke, p = .8,
                                  list = FALSE, 
                                  times = 1)

train <- df[trainIndex, ]
test <- df[-trainIndex, ]
table(train$stroke)
table(test$stroke)

fit <- glm(stroke~., data=train,family='binomial')

summary(fit)

```
Fit the splits to the same features

```{r}
dmy <- dummyVars(" ~ .", data = train, fullRank = TRUE)
trsf <- data.frame(predict(dmy, newdata = train)) %>% dplyr::select(-id)

trsf$stroke <- make.names(trsf$stroke) %>% as.factor()
trsf %>% head()
```

```{r}
dmy_test <- dummyVars(" ~ .", data = test, fullRank = TRUE)
trsf_test <- data.frame(predict(dmy, newdata = test)) %>% dplyr::select(-id)

trsf_test$stroke <- make.names(trsf_test$stroke) %>% as.factor()

```
down sampling

```{r}
down_train <- downSample(x = trsf[, -ncol(trsf)],
                         y = trsf$stroke)
colnames(down_train)[16] <- "stroke"
table(down_train$stroke)   
```
 up sampling
```{r}
up_train <- upSample(x = trsf[, -ncol(trsf)],
                     y = trsf$stroke)   
colnames(up_train)[16] <- "stroke"

table(up_train$stroke) 
```
smote
```{r}
smote_train <- SMOTE(stroke ~ ., data  = trsf)

table(smote_train$stroke) 
```
rose
```{r}
rose_train <- ROSE(stroke ~ ., data  = trsf)$data
table(rose_train$stroke) 

```

```{r}
input_list <- list(trsf, up_train, down_train, rose_train, smote_train)
test_list <- list(trsf_test, trsf_test, trsf_test, trsf_test, trsf_test)

test_func <- function(x){
 y = train(stroke ~ ., data = x, 
                       method = "treebag",
                       nbagg = 50,
                       metric = "ROC",
                       trControl = trainControl(method = "repeatedcv", 
                                                number = 2,
                                                classProbs = TRUE,
                                                summaryFunction = twoClassSummary,
                                                repeats = 1))
 y
}

output_list <- purrr::map(input_list, test_func)

test_roc <- function(model, data) {
  library(pROC)
  roc_obj <- roc(data$stroke, 
                 predict(model, data, type = "prob")[, "X0"],
                 levels = c("X1", "X0"))
  ci(roc_obj)
}

outside_models <- list(original = output_list[[1]],
                       down = output_list[[3]],
                       up = output_list[[2]],
                       SMOTE = output_list[[5]],
                       ROSE = output_list[[4]])

outside_resampling <- resamples(outside_models)

outside_test <- lapply(outside_models, test_roc, data = trsf_test)
outside_test <- lapply(outside_test, as.vector)
outside_test <- do.call("rbind", outside_test)
colnames(outside_test) <- c("lower", "ROC", "upper")
outside_test <- as.data.frame(outside_test)

summary(outside_resampling, metric = "ROC")
outside_test
```


```{r}
fitControl <- trainControl(method = "repeatedcv",
                           number = 10,
                           repeats = 10,
                           classProbs = TRUE,
                           summaryFunction = twoClassSummary,
                           search = "random")


cl <- makePSOCKcluster(5)
registerDoParallel(cl)
rda_fit <- train(stroke ~ ., data = smote_train, 
                  method = "rda",
                  metric = "ROC",
                  tuneLength = 30,
                  trControl = fitControl)
stopCluster(cl)

rda_fit
summary(test_roc(rda_fit, trsf_test))

```

```{r}
cl <- makePSOCKcluster(5)
registerDoParallel(cl)

rf_fit <- train(stroke ~ ., data = smote_train, 
                  method = "ranger",
                  metric = "ROC",
                  tuneLength = 30,
                  trControl = fitControl)
stopCluster(cl)

rf_fit
summary(test_roc(rf_fit, trsf_test))

```


```{r}
cl <- makePSOCKcluster(5)
registerDoParallel(cl)


nb_fit <- train(stroke ~ ., data = smote_train, 
                  method = "naive_bayes",
                  metric = "ROC",
                  tuneLength = 30,
                  trControl = fitControl)

## When you are done:
stopCluster(cl)

nb_fit

summary(test_roc(nb_fit, trsf_test))
```

